--solution.sql


--1
SELECT customerName 
FROM customers 
WHERE country = "Philippines";

--2
SELECT customerName 
FROM customers 
WHERE country = "USA";

--3
SELECT contactLastName, contactFirstName 
FROM customers 
WHERE customerName = "La Rochelle Gifts";

--4
SELECT productName, MSRP 
FROM products 
WHERE productName = "The Titanic";

--5
SELECT firstName, lastName 
FROM employees 
WHERE email = "jfirrelli@classicmodelcars.com";

--6
SELECT customerName, state 
FROM customers 
WHERE state IS NULL;

--7
SELECT firstName, lastName, email 
FROM employees 
WHERE lastName = "Patterson" 
	AND firstName = "Steve";

--8
SELECT customerName, country, creditLimit 
FROM customers
WHERE country != "USA" 
	AND creditLimit > 3000;

--9
SELECT customerNumber
FROM orders
WHERE comments LIKE "%DHL%";

--10
SELECT productLine
FROM productlines
WHERE textDescription LIKE "%state of the art%";

--11
SELECT DISTINCT country 
FROM customers; 

--12
SELECT DISTINCT status
FROM orders;

--13
SELECT customerName, country
FROM customers
WHERE country IN ("USA","France","Canada");

--14
SELECT 
	employees.firstName, 
	employees.lastName,
	offices.city AS city_of_office
FROM employees
JOIN offices
	ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

--15
SELECT 
	customers.customerName
FROM customers
JOIN employees
	ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE CONCAT(TRIM(employees.firstName), ' ', TRIM(employees.lastName))
LIKE '%Leslie Thompson%';

--16
SELECT productName, quantityInStock
FROM products 
WHERE productLine = "Planes" 
	AND quantityInStock < 1000;

--17
SELECT customerName
FROM customers
WHERE phone LIKE "+81%";

--18
SELECT count(*) AS number_of_customers
FROM customers
WHERE country = "UK";


/* STRETCH GOALS */



/*
	CUSTOMERS
	`customerNumber`, 
	`customerName`, 
	`contactLastName`, 
	`contactFirstName`, 
	`phone`, 
	`addressLine1`, 
	`addressLine2`, 
	`city`, 
	`state`, 
	`postalCode`, 
	`country`, 
	`salesRepEmployeeNumber`, 
	`creditLimit`

	EMPLOYEES
	`employeeNumber`, 
	`lastName`, 
	`firstName`, 
	`extension`, 
	`email`, 
	`officeCode`, 
	`reportsTo`, 
	`jobTitle`

	OFFICES
	`officeCode`, 
	`city`, 
	`phone`, 
	`addressLine1`, 
	`addressLine2`, 
	`state`, 
	`country`, 
	`postalCode`, 
	`territory`

	ORDER DETAILS
	`orderNumber`, 
	`productCode`, 
	`quantityOrdered`, 
	`priceEach`, 
	`orderLineNumber`

	ORDERS
	`orderNumber`, 
	`orderDate`, 
	`requiredDate`, 
	`shippedDate`, 
	`status`, 
	`comments`, 
	`customerNumber`

	PAYMENTS
	`customerNumber`, 
	`checkNumber`, 
	`paymentDate`, 
	`amount`

	PRODUCTLINES
	`productLine`, 
	`textDescription`, 
	`htmlDescription`, 
	`image`

	PRODUCTS
	`productCode`, 
	`productName`, 
	`productLine`, 
	`productScale`, 
	`productVendor`, 
	`productDescription`, 
	`quantityInStock`, 
	`buyPrice`, 
	`MSRP`
*/

--1
SELECT 
	products.productName, 
	customers.customerName
FROM products
JOIN orderdetails
	ON products.productCode = orderdetails.productCode
JOIN orders
	ON orderdetails.orderNumber = orders.orderNumber
JOIN customers
	ON orders.customerNumber = customers.customerNumber
WHERE customerName = "Baane Mini Imports";

-- result: 32 rows

--2
SELECT
	ROW_NUMBER() OVER(ORDER BY employees ASC) AS rownum,
	CONCAT(firstName, " ",lastName) AS employees,
   jobTitle
FROM employees
WHERE jobTitle = "Sales Rep"
	AND officeCode =
	(SELECT officeCode 
     FROM employees
     WHERE CONCAT(TRIM(firstName), ' ', TRIM(lastName))
		LIKE '%Anthony Bow%');


/*RESULT

rownum	employees			jobTitle	
1			Leslie Jennings	Sales Rep	
2			Leslie Thompson	Sales Rep	

*/


--3
SELECT 
	productName,
    MAX(MSRP) AS highest_MSRP
FROM products
GROUP BY MSRP DESC LIMIT 1;

/* RESULT 

productName						highest_MSRP
1952 Alpine Renault 1300	214.30

*/


--4
SELECT 
	productLine,
    count(DISTINCT productName) AS products
FROM products  
GROUP BY productLine
ORDER BY products DESC;

/* RESULTS

productLine			products   	
Classic Cars		38	
Vintage Cars		24	
Motorcycles			13	
Planes				12	
Trucks and Buses	11	
Ships					9	
Trains				3	

*/


--5
SELECT count(*) AS Cancelled_Orders
FROM orders 
WHERE status = "Cancelled";

/* RESULTS

Cancelled_Orders
6	

*/